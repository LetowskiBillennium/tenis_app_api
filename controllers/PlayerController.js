const router = require('express').Router();
const players = require('../responseMocks/players');
const newPlayerValidator = require('../requestValidators/newPlayerValidator');
const { check, validationResult } = require('express-validator');

const routes = {
  base: '/',
}

router.get(routes.base, (req, res) => {
    res.status(200).json(players);
  });

router.post(routes.base, newPlayerValidator, (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.array() });
  }
    res.status(201).json(players);
  });

module.exports = router;
