# Tenis app api

## Enviroment configuration:
  - Run command: npm install.
  - When packes are installed run: npm run dev.
  - Application should run on port 8081.

## Add new api endpoints to api:
  - If you need to add new controller, you should place it in the directory: controllers.
  - All new controllers have to be registered in index.js (like PlayerController).
  - Basic controller structure has been described in PlayerController.
  - If you need to validate query or body you can use lib: https://express-validator.github.io/docs/index.html. Sample validation is described in the directory requestValidators in the file newPlayerValidator.
  - Response's mocks should be placed in responseMocks.
  - There is also request.http file. If you use VS Code and you want to test your endpoint, you can install extension REST Client and to request.http file add simple test implementation.