const express = require('express');
const bodyParser = require('body-parser');
const app = express();

const headersMiddleware = require('./middlewares/headersMiddleware');
const { port } = require('./utils/config');
const globalRoutes = require('./utils/globalRoutes');

const players = require('./controllers/PlayerController');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(headersMiddleware);

app.use(globalRoutes.players, players);

app.listen(port, () => {
  console.log(`Listening to requests on http://localhost:${port}`);
});