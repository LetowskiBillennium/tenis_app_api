const { body } = require('express-validator');

module.exports = [
  body('email')
    .isEmail()
    .normalizeEmail(),
  body('text')
    .not().isEmpty()
    .trim()
    .escape(),
  body('notifyOnReply').toBoolean()
]